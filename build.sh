#!/bin/sh
# prerequisites : luaJit

LOVE_DOWNLOAD_LINK="https://github.com/love2d/love/releases/download/11.3/love-11.3-win64.zip"
FILE_NAME_ZIP=${LOVE_DOWNLOAD_LINK##*/}
FILE_NAME=${FILE_NAME_ZIP%.*}
WD=`pwd`

# (1) prepare build
rm -rf build love-android-extensions/app/src/main/assets/game.love
mkdir -p build
cd build
mkdir LOVE exe android
wget $LOVE_DOWNLOAD_LINK
cd LOVE
unzip ../${FILE_NAME_ZIP}
cd $FILE_NAME && mv ./* ../ && cd .. && rm -rf $FILE_NAME
cd ..
rm -rf $FILE_NAME_ZIP
cd ..

# (2) make love file
TEMP=`mktemp -d` # create a temporary directory
#cp -Rv ./* $TEMP # copy your source code to temp
rsync -av --progress ./* $TEMP --exclude love-android-extensions
cd $TEMP # move to temp
rm -rf build
# obfuscate the code
#for file in $(find . -iname "*.lua") ; do
# luajit -b ${file} ${file}
#done

zip -9 -r ./build.zip ./* -x ./build.sh --exclude=*build* --exclude=*love-android-extensions*

mv build.zip game.love
mv game.love $WD/build
cd $WD/build

# (3) build windows executable
cat LOVE/love.exe ./game.love > exe/game.exe
cp -r LOVE/* ./exe
cd exe
rm changes.txt readme.txt lovec.exe love.exe
cd ../.. 

# (4) build android apk
mkdir love-android-extensions/app/src/main/assets
rm -rf love-android-extensions/app/src/main/assets/game.love
cp build/game.love love-android-extensions/app/src/main/assets

cd love-android-extensions

./gradlew assembleEmbed
mv app/build/outputs/apk/embed/debug/app-embed-debug.apk app/build/outputs/apk/embed/debug/game.apk
cp app/build/outputs/apk/embed/debug/game.apk $WD/build/android

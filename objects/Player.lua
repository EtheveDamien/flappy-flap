local PhysicEntity = require("objects/PhysicEntity")
local Vector = require("objects/Vector")

local Player = {}
Player.__index = Player

-- setup inheritance
setmetatable(Player, {__index = PhysicEntity})

local jumpForce = 120

function Player:new(pX, pY)
  local image = love.graphics.newImage("assets/spritesheets/birdc.png")
  local self = PhysicEntity:new(pX, pY, image, 5, 3)
  self:addAnim("idle", 6, 10, 0.6)
  self:addAnim("run", 11,15, 0.3)
  self:addAnim("jump", 1, 5, 0.3)
  self:addAnim("fall", 1, 5, 0.8)
  self:playAnim("idle")
  self.isColideUp = true
  self.goLeft = false
  self.goRight = false
  self.goUp = false
  self.maxSpeedY = 500
  self.maxJump = 3
  self.currentJump = 3

  -- super methods
  self.superPhysicEntityUpdate = self.update

  setmetatable(self, Player)
  return self
end

function Player:update(dt)
  --print(self.isGround, self.isColideUp)
  self.isKeyPressed = self.goLeft

  -- animations
  if self.isGround == true and math.abs(self.velocity.x) >= 50 and math.abs(self.velocity.y) >= 0 and math.abs(self.velocity.y) <= 1 then
    self:playAnim("run")
    love.audio.play(soundRun)
  elseif self.isGround == true and math.abs(self.velocity.x) < 50 and math.abs(self.velocity.y) >= 0 and math.abs(self.velocity.y) <= 1  then
    love.audio.stop(soundRun)
    self:playAnim("idle")
    love.audio.stop(soundRun)
  elseif self.velocity.y <= 2 and self.isGround == false then
    self:playAnim("jump")
    love.audio.stop(soundRun)
    if self.velocity.x > 0 then  self.angle = self.angle  - 1 else self.angle = self.angle + 1 end
   
  elseif self.velocity.y >= 2 then
    self:playAnim("fall")
    love.audio.stop(soundRun)
    if self.velocity.x > 0 then  self.angle = self.angle  - 1 else self.angle = self.angle + 1 end
  else
    self:playAnim("idle")
    love.audio.stop(soundRun)
  end

  -- keyboard inputs
  --if love.keyboard.isDown("left") then
  if self.goLeft or love.keyboard.isDown("left") then
    self.velocity.x = self.velocity.x - self.speed
    self.scale.x = - math.abs(self.scale.x)
    self.isKeyPressed = true
  end
  -- if love.keyboard.isDown("right") then
  if self.goRight or love.keyboard.isDown("right") then
    self.velocity.x = self.velocity.x + self.speed
    self.scale.x = math.abs(self.scale.x)
    self.isKeyPressed = true
  end

  if self.goUp and self.isGround and self.isColideUp == false then
    self.velocity.y = -jumpForce
    self.isGround = false
    self.angle = 0
    self.goUp = false
    self.currentJump = self.currentJump - 1
    love.audio.stop(soundJump)
    love.audio.play(soundJump)
  end

  if self.goUp and self.currentJump > 0 then
    self.velocity.y = -jumpForce
    self.isGround = false
    self.angle = 0
    self.goUp = false
    self.currentJump = self.currentJump - 1
    love.audio.stop(soundJump)
    love.audio.play(soundJump)
  end

  self:superPhysicEntityUpdate(dt)
end

function Player:keypressed(key)
  if key == "space" and self.isGround and self.isColideUp == false then
    self.velocity.y = -jumpForce
    self.angle = 0
    self.isGround = false
    love.audio.stop(soundJump)
    love.audio.play(soundJump)
  end

  if key == "space" and self.currentJump > 0 then
    self.velocity.y = -jumpForce
    self.isGround = false
    self.angle = 0
    self.currentJump = self.currentJump - 1
    love.audio.stop(soundJump)
    love.audio.play(soundJump)
  end
end

function Player:keyreleased(key)
end

return Player

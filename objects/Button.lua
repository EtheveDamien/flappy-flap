local Vector = require("objects/Vector")
local isMouseInBox = require("utils/Utils").isMouseInBox
local Font = require("utils/Font")

local Button = {}
Button.__index = Button

function Button:new(pId, pPosition, pDimension, pCallbackPressed, pCallbackReleased)
  local self = {}
  self.id = pId
  self.position = pPosition
  self.dimension = pDimension
  self.callbackPressed = pCallbackPressed or function () end
  self.callbackReleased = pCallbackReleased or function () end
  self.isPressed = false

  setmetatable(self, Button)
  return self
end

function Button:setPosition(position)
  self.position = position
end

function Button:draw()
  
  if self.isPressed then
    love.graphics.rectangle(
      "fill",
      self.position.x,
      self.position.y,
      self.dimension.x,
      self.dimension.y
    )
  else
    love.graphics.rectangle(
      "line",
      self.position.x,
      self.position.y,
      self.dimension.x,
      self.dimension.y
    )
  end

  if self.id ~= nil then 
    local tw = Font.main:getWidth(self.id)
    local th = Font.main:getHeight(self.id)
    love.graphics.print(
      self.id,
      Font.main,
      self.position.x + self.dimension.x/2  - tw/2,
      self.position.y + self.dimension.y/2 - th/2
    )
  end
end

function Button:mousepressed(x, y)
  local isInside = isMouseInBox(
    x,y, 
    self.position.x, self.position.y, 
    self.dimension.x, self.dimension.y
  )
  if isInside then
    self.callbackPressed()
    self.isPressed = true
  end
end

function Button:mousereleased(x, y)
  if self.isPressed then
    self.callbackReleased()
    self.isPressed = false
  end
end

function Button:touchpressed()
  -- ici c'est chian
  -- en gros on stock les touches dans me main.lua
  -- on n'utilise plus les parametres mais on get les id des touches dans le release :)
  for i,v in pairs(touches) do
    local isInside = isMouseInBox(
      v.x,v.y, 
      self.position.x, self.position.y, 
      self.dimension.x, self.dimension.y
    )
    if isInside then
      self.callbackPressed()
      self.isPressed = true
      self.touchId = i
    end
  end
end

function Button:touchreleased()
  -- vérifie bien que l'ID de touch est bien a nil (voir main)
  if self.isPressed == true and touches[self.touchId] == nil then
    self.callbackReleased()
    self.isPressed = false
  end
end

return Button

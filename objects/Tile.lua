local Vector = require("objects/Vector")
local Sprite = require("objects/Sprite")

local Tile = {}
Tile.__index = Tile

-- setup inheritance
setmetatable(Tile, {__index = Sprite})

function Tile:new(pX, pY, pTileSheet, pQuad, pNum, id, tileNumber)
  local self = Sprite:new(pX, pY, pTileSheet, tileNumber.x, tileNumber.y)
  self.id = id
  self.quad = pQuad
  self.num = pNum
  self.isSelected = false
  self.isSolid = false

  -- super methods
  self.superSpriteDraw = self.draw

  setmetatable(self, Tile)
  return self
end

function Tile:update(dt)
end

function Tile:remove()
  self.quad = nil
  self.num = 0
end

function Tile:draw()
  if self.quad ~= nil then
    love.graphics.draw(
      self.spriteSheet,
      self.quad,
      self.position.x * self.scale.x,
      self.position.y * self.scale.y,
      self.angle,
      self.scale.x,self.scale.y
    )
  end
  if self.isSelected then
    self:superSpriteDraw()
  end
end

return Tile

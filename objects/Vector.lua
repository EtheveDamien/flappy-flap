local Vector = {}
Vector.__index = Vector

function Vector:new(pX, pY)
  local self = {}
  self.x = pX or 0 
  self.y = pY or 0
  setmetatable(self, Vector)
  return self
end

function Vector:print()
  print(" x : "..self.x .. " | y : "..self.y)
end

return Vector

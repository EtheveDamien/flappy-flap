return {
  isMouseInBox = function (x, y, bx, by, bw, bh)
    if  
        x ~= nil and
        bx ~= nil and
        by ~= nil and
        y ~= nil and
        x > bx and
        x < bx + bw and
        y > by and
        y <  by + bh then
      return true
    else
      return false
    end
  end,
  isBoxInBox = function (x1,y1,w1,h1, x2,y2,w2,h2)
    return x1 < x2+w2 and
      x2 < x1+w1 and
      y1 < y2+h2 and
      y2 < y1+h1
  end
}

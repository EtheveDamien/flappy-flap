local Font = {}

Font.title = love.graphics.newFont("assets/fonts/prstart.ttf", 60)
Font.main = love.graphics.newFont("assets/fonts/prstart.ttf", 19)
Font.small =  love.graphics.newFont("assets/fonts/prstart.ttf", 11)

return Font

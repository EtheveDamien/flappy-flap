local Button = require("objects/Button")
local Vector = require("objects/Vector")
local Slider = require("objects/Slider")
local Font = require("utils/Font")

local Menu = {}
local play = nil
local slider = nil
local r = 0
local birdImg = nil

local buttonW, buttonH = 150, 60
local gameName = "FLAPPY FLAP"

function Menu.load()
  love.graphics.setBackgroundColor(1,1,1,1)
  birdImg = love.graphics.newImage("assets/spritesheets/birdc-solo.png")

  play = Button:new(
    "Play",
    Vector:new(WIDTH/2 - buttonW/2, HEIGHT/2 - buttonH/2),
    Vector:new(buttonW,buttonH), 
    function() end,
    function() sm:setScene("Levels") end
  )

  quit = Button:new(
    "Quit",
    Vector:new(WIDTH/2 - buttonW/2, HEIGHT-HEIGHT/3 - buttonH/2),
    Vector:new(buttonW,buttonH), 
    function() end,
    function() love.event.quit() end
  )
end

sens = 0
function Menu.update(dt)
  print(r)
  sens = sens + dt * 2
  r =  math.cos(sens) 
end

function Menu.draw()
  love.graphics.setColor(0,0,0,1)
  local tw = Font.title:getWidth(gameName)
  local th = Font.title:getHeight(gameName)
  love.graphics.print(
    gameName,
    Font.title,
    WIDTH/2 - tw/2,
    HEIGHT/5 - th/2,
    0,
    1,1,r*5,r*5
  )
  play:draw()
  quit:draw()

  love.graphics.setColor(1,1,1  ,1)
  love.graphics.draw(birdImg, WIDTH - WIDTH/4, HEIGHT - HEIGHT/4, r, 7,7, birdImg:getWidth()/2, birdImg:getHeight()/2)
  -- love.graphics.setColor(1,0,0,1)
  -- love.graphics.line(WIDTH/2, 0, WIDTH/2, HEIGHT)
  -- love.graphics.line(0, HEIGHT/2, WIDTH, HEIGHT/2)
end

function Menu.keypressed(key)
  sm:setScene("Levels")
end

function Menu.mousepressed(x, y, button, istouch, presses)
  play:mousepressed(x, y)
  quit:mousepressed(x, y)
end

function Menu.mousereleased(x, y, button, istouch, presses)
  play:mousereleased(x, y)
  quit:mousereleased(x, y)
end

return Menu

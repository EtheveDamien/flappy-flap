local Button = require("objects/Button")
local Vector = require("objects/Vector")
local isMouseInBox = require("utils/Utils").isMouseInBox
local Font = require("utils/Font")

local Levels = {}

local lockImg = love.graphics.newImage("assets/ui/lock.png")
local case = {}
local buttonW, buttonH = 150, 60

function Levels.load()
  love.graphics.setBackgroundColor(1,1,1,1)
  CW = 60
  CH = 60

  --boutton
  back = Button:new(
    "Back",
    Vector:new(WIDTH/2 - buttonW/2, HEIGHT-HEIGHT/4 - buttonH/2),
    Vector:new(buttonW,buttonH), 
    function() end,
    function() sm:setScene("Menu") end
  )


  case = {} -- reset a chaque fois !!
  local entre = 5
  local n = 0
  -- put it on the center WIP
  --(5 * (CH+entre) / HEIGHT)
  -- ((9 * (CW+entre)) / WIDTH)
  for i=0,1 do
    for j=0 ,4 do
      n = n+1
      local b = {} 
      if n <= MAX_LEVEL then
        b.isLock = false
      else
        b.isLock = true
      end
      b.num = tostring(n)
      b.button = Button:new(
        tostring(n),
        Vector:new(
          ((WIDTH/2 - (4 * entre  + 5 * CW )/2  ) + j*(CW+entre)),
          (HEIGHT/2 - (1 * entre  + 2 * CH )/2  ) + i*(CH+entre)
        ),
        Vector:new(CW, CH),
        function()
        end,
        function()
          ACTUAL_LEVEL = tonumber(b.num)
          sm:setScene("Game", b.num)
        end
      )
      table.insert(case, b)
    end
  end
end

function Levels.update()
  --[[
  local mx, my = love.mouse.getPosition()

    for i,v in ipairs(case) do
      local collide = isMouseInBox(
        mx,my,
        v.button.position.x, v.button.position.y,
        v.button.dimension.x, v.button.dimension.y
      )
      if collide then
        if v.button.dimension.x < CW / 1.1 then
          v.button.dimension.x = CW / 1.1
        else
          v.button.dimension.x = v.button.dimension.x - 0.5
        end

        if v.button.dimension.y < CH / 1.1 then
          v.button.dimension.y = CH / 1.1
        else
          v.button.dimension.y = v.button.dimension.y - 0.5
        end
      else
        v.button.dimension.x = CW
        v.button.dimension.y = CH
      end
    end
  ]]
end

function Levels.draw()
  love.graphics.setColor(0,0,0,1)

  local tw = Font.title:getWidth("Levels")
  local th = Font.title:getHeight("Levels")
  love.graphics.print(
    "Levels",
    Font.title,
    WIDTH/2 - tw/2,
    HEIGHT/5 - th/2
  )

  for i,v in ipairs(case) do
    if v.isLock then
      love.graphics.draw(
        lockImg,
        v.button.position.x, v.button.position.y, 
        0, 
        0.05, 0.05,
        v.button.dimension.x/2, v.button.dimension.y/2
      )
    end
    v.button:draw()
  end
  back:draw()
  -- love.graphics.setColor(1,0,0,1)
  -- love.graphics.line(WIDTH/2, 0, WIDTH/2, HEIGHT)
  -- love.graphics.line(0, HEIGHT/2, WIDTH, HEIGHT/2)
end

function Levels.mousepressed(x, y, button, istouch, presses)

  for i,v in ipairs(case) do
    if v.isLock == false then
      v.button:mousepressed(x, y)
    end
  end

  back:mousepressed(x, y, button, istouch, presses)
end

function Levels.mousereleased(x, y, button, istouch, presses)
  for i,v in ipairs(case) do
    if v.isLock == false then
      v.button:mousereleased(x, y)
    end
  end

  back:mousereleased(x, y, button, istouch, presses)
end

return Levels

local Player = require("objects/Player")
local MM = require("handlers/MapManager")
local MCM = require("handlers/MapCollideManager")
local Camera = require("objects/Camera")
local Slider = require("objects/Slider")
local Sprite = require("objects/Sprite")
local Vector = require("objects/Vector")
local Box = require("objects/PhysicEntity")
local Controller = require("handlers/TouchController")
local isMouseInBox = require("utils/Utils").isMouseInBox
local Font = require("utils/Font")

local Game = {}

function Game.load(mapNumber)
  SLOWMOTION = false
  love.graphics.setBackgroundColor(255/255, 255/255, 255/255)
  mm = MM:new()
  cam = Camera:new()
  mm:changeMap(mapNumber)
  startTile = mm:getStartTile()
  deadTiles = mm:getDeadTiles()
  endTile = mm:getEndTile()
  p = Player:new(startTile.position.x, startTile.position.y)


  -- setup bonus
  bonus = 0
  totalBonus = 3
  lstBonus = {}
  for i=1,totalBonus do
    bon = Sprite:new((WIDTH/2 + (i-1) * TW) - totalBonus*(TW/2) +  (3* TW) / 2 , 30, love.graphics.newImage("assets/spritesheets/life.png"), 3, 4)
    bon:addAnim("bon_on", 11, 11, 1)
    bon:addAnim("bon_off", 10, 10, 1)
    bon:playAnim("bon_off")
    lstBonus[i] = bon
  end

  -- setup life
  life = 3
  lstLife = {}
  for i=1,life do
    coeur = Sprite:new((WIDTH/2 + (i-1) * TW) - life*(TW/2) - (3 * TW) / 2, 30, love.graphics.newImage("assets/spritesheets/life.png"), 3, 4)
    coeur:addAnim("vie_on", 5, 5, 1)
    coeur:addAnim("vie_off", 4, 4, 1)
    coeur:playAnim("vie_on")
    lstLife[i] = coeur
  end
  
  cam:reScale(0.3,0.3)
  mcm = MCM:new(mm)
  controller = Controller:new(
    function (dt)
      p.goLeft = true
    end,
    function (dt)
      p.goLeft = false
    end,
    function (dt)
      p.goRight = true
    end,
    function (dt)
      p.goRight = false
    end,
    function (dt)
      p.goUp = true
    end,
    function (dt)
      p.goUp = false
    end
  )
  mcm:addElement(
    p,
    Vector:new(0,0) -- rescale colider box
  )
end

time=5
function Game.update(dt)
  -- check colision with end
  if endTile ~= nil and isMouseInBox(p.position.x + p.dimension.x /2, p.position.y + p.dimension.y /2, endTile.position.x, endTile.position.y, endTile.dimension.x, endTile.dimension.y) then
    -- next level
    SLOWMOTION = true
    -- p.velocity = Vector:new(0,0)
    --p.position = endTile.position
    -- sm:setScene("Menu")

    if MAX_LEVEL <= ACTUAL_LEVEL then
      MAX_LEVEL = ACTUAL_LEVEL + 1
    end

    
    sm:setScene("Levels")
    print("COLIDE end")
    endTile = nil -- callback tips and triks lol
  end

  -- check colision with bonus
  for i,v in ipairs(mm.bonuss) do
    if isMouseInBox(p.position.x + p.dimension.x /2, p.position.y + p.dimension.y /2, v.position.x, v.position.y, v.dimension.x, v.dimension.y) then
      print("COLIDE BONUS")
      table.remove(mm.bonuss, i)
      bonus = bonus + 1
      lstBonus[bonus]:playAnim("bon_on")
    end
  end

  -- check colision with flap
   for i,v in ipairs(mm.boosts) do
    if isMouseInBox(p.position.x + p.dimension.x /2, p.position.y + p.dimension.y /2, v.position.x, v.position.y, v.dimension.x, v.dimension.y) then
      print("FLAP")
      p.currentJump = p.currentJump + 1
      table.remove(mm.boosts, i)
    end
  end
  
  -- IF DEAD
  if life == 0 then
    sm:setScene("Menu")
    life = 3
    bonus = 0
    for i=1,life do
      lstLife[i]:playAnim("vie_on")
    end
  end

  dtslow = dt
  if SLOWMOTION then
    time = time - dt
    dtslow = dt/4
    if time < 0 then
      time = 5 -- make it stop at 0 in the sloppiest way possible
      SLOWMOTION = false
    end
  end
  p:update(dtslow)


  controller:update(dt)

  local dx = cam.position.x - (p.position.x - WIDTH/2  * cam.scale.x)  - p.dimension.x/2
  local dy = cam.position.y - (p.position.y - HEIGHT/2  * cam.scale.y ) - p.dimension.y/2
  cam:setPosition(
    cam.position.x - (dx * 0.06), --0.06
    cam.position.y - (dy * 0.06)
  )
  mcm:update(dt)

  if p.position.y >= mm.mapFile.layers[1].height * TH then
    p.position.x = startTile.position.x
    p.position.y = startTile.position.y
    p.velocity = Vector:new(0,0)

    lstLife[life]:playAnim("vie_off")
    life = life - 1
    -- reset bonus
    bonus = 0
    for k,v in pairs(lstBonus) do
      v:playAnim("bon_off")
    end

    mm:changeMap(ACTUAL_LEVEL) -- reset the level
    p.currentJump = 3
  end


  for i,v in ipairs(deadTiles) do
    if p:isCollide(v) or p.position.y >= mm.mapFile.layers[1].height * TH then
      p.position.x = startTile.position.x
      p.position.y = startTile.position.y
      p.velocity = Vector:new(0,0)

      lstLife[life]:playAnim("vie_off")
      life = life - 1
      mm:changeMap(ACTUAL_LEVEL) -- reset the level
      p.currentJump = 3
    end
  end

  
  mm:update(dt)
  
end

function Game.draw()
  cam:set()

  mm:drawBack()
  p:draw()
  mm:draw()
  
  -- mcm:draw()

  cam:unset()
  for k,v in pairs(lstLife) do
    v:draw()
  end
  for k,v in pairs(lstBonus) do
    v:draw()
  end
  controller:draw()
  love.graphics.setColor(0,0,0,0.8)
  -- slider:draw()

  local fps = "fps: "..tostring(love.timer.getFPS( ))
  local tw = Font.small:getWidth(fps)
  local th = Font.small:getHeight(fps)
  love.graphics.print(
    fps,
    Font.small,
    WIDTH - 6*TW, 30
  )
  -- love.graphics.print("fps: "..tostring(love.timer.getFPS( )), WIDTH - 6*TW, 30)
  local currjump = tostring(p.currentJump)
  local tw = Font.main:getWidth(currjump)
  local th = Font.main:getHeight(currjump)
  love.graphics.print(
    currjump,
    Font.main,
    WIDTH/2 - tw/2, HEIGHT/3 - th/2
  )

  --love.graphics.setColor(1,0,0,1)
  --love.graphics.line(WIDTH/2, 0, WIDTH/2, HEIGHT)
  --love.graphics.line(0, HEIGHT/2, WIDTH, HEIGHT/2)
  love.graphics.setColor(1,1,1,1)

end

function Game.keypressed(key)
  p:keypressed(key)
  if(key == "escape") then sm:setScene("Menu") end
end

function Game.keyreleased(key)
  p:keyreleased(key)
end

function Game.mousepressed(x,y)
  controller:mousepressed(x,y)
  --if a == false and b == false and c == false then
    --[[
      local mx, my = cam:getMousePosition(x,y)
      local tile = mm:getTileAt(Vector:new(mx, my))
      tile.num = 2
      tile.isSelected = true
      tile.isSolid = true
    ]]
  --end
end

function Game.mousereleased(x,y)
  controller:mousereleased(x,y)
end


function Game.touchpressed(id, x, y, dx, dy, pressure )
  controller:touchpressed(id, x, y, dx, dy, pressure )
end

function Game.touchreleased(id, x, y, dx, dy, pressure )
  controller:touchreleased(id, x, y, dx, dy, pressure )
end

return Game

## Get started


### Clone
```shell
git clone --recurse-submodules -j8 git@gitlab.com:love2d2/flappy-flap.git
```

## AdMob

### Love2D configuration

From now, you will needs to run your game in an emulator on on a physical device.

1) Add `love_admob.lua` file in your project
2) Redefine the run function in the `main.lua` file.
```lua
local love_admob = require("utils.love_admob")

function love.run()
	if love.load then love.load(love.arg.parseGameArguments(arg), arg) end
	if love.timer then love.timer.step() end

	local dt = 0

	return function()
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a or 0
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end
		if love.timer then dt = love.timer.step() end
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
		if love.graphics and love.graphics.isActive() then
			love.graphics.origin()
			love.graphics.clear(love.graphics.getBackgroundColor())
			if love.draw then love.draw() end
			love.graphics.present()
		end

		if love_admob then love_admob.update(dt) end

		if love.timer then love.timer.sleep(0.001) end
	end
end
```
3. Now you can use the admob [API](https://love2d.org/forums/viewtopic.php?t=84226).


### Android configuration

1) Clone this repository : https://github.com/flamendless/love-android-extensions (LÖVE you so much <3)

```shell
git clone --recurse-submodules git@github.com:flamendless/love-android-extensions.git
```

2) Edit the gradle.properties

```properties
android.enableJetifier=true
android.useAndroidX=true
org.gradle.daemon=false

app.name=FLAPPY_FLAP
app.activity_name=org.flamendless.admob.AdActivity
app.orientation=portrait

flamendless.admob=true

admob.app_id=ca-app-pub-3290184253825454~7555594842
admob.publisher_id=pub-3290184253825454
admob.privacy_url=https://www.google.com/about/company/user-consent-policy/
admob.test_device_id=7f2fb1da-cf74-4bb6-b3de-3b8d7d2edbdd
admob.collect_consent=true
```

3) Edit the `version_code`, `version_name` and `applicationId` in the `love-android-extensions/app/build.gradle` file.

4) Run `./gradlew assembleEmbed` or `./build.sh`

## Class diagram

```mermaid
classDiagram
	Entity <|-- Sprite
	Entity <|-- Point

	Animator o-- Vector
	
	Sprite o-- Vector
	Sprite o-- Animator
	Sprite <|-- PhysicEntity
	Sprite <|-- Tile

	PhysicEntity <|-- Enemy:todo
	PhysicEntity <|-- Player
	PhysicEntity <|-- Object:todo

	ProgressBar o-- Vector

	MapManager o-- Vector
	MapManager o-- Tile
	
	class Camera{
	}

	class Slider{
	}

	class Entity{
		vector position
		vector velocity
		new(x, y)
		update(dt)
	}

	class Vector{
		number x
		number y
		new(x, y)
		print()
	}
	
	class Point{
		getDistanceFrom(other)
		getAngleFrom(other)
		new(x, y)
		draw()
	}
	
	class Animator{
		image spriteSheet
		vector numAnim
		vector nbSprite
		number duration
		number currentTime
		boolean isRunning
		table quads
		new(spriteSheet, nbSprite, numAnim, duration)
		getDistanceFrom(other)
		getAngleFrom(other)
		draw()
	}
	
	
	class Sprite{
		number angle
		vector scale
		vector nbImageInTheSpritesheet
		table anims
		image spriteSheet
		string currentAnim
		vector dimension
		new(x, y, spriteSheet, nbX, nbY)
		addAnim()
		playAnim()
		update(dt)
		draw()
	}
```

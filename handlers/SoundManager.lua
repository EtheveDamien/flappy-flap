local SoundManager = {}
SoundManager.__index = SoundManager

function SoundManager:new(pInitial)
  local self = {}
  
  setmetatable(self, SoundManager)
  return self
end

return SoundManager

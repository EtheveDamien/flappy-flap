local Vector = require("objects/Vector")
local Tile = require("objects/Tile")
local isMouseInBox = require("utils/Utils").isMouseInBox
local Sprite = require("objects/Sprite")

local MapManager = {}
MapManager.__index = MapManager

function MapManager:new(pX, pY)
  local self = {}
  self.tilesheet = nil
  self.mapFile = nil
  self.level = nil
  self.lstQuad = {}
  self.lstTile = {}
  self.actualMap = {}

  setmetatable(self, MapManager)
  return self
end

function MapManager:changeMap(_num)
  -- reset map
  self.tilesheet = nil
  self.level = nil
  self.flyboost = nil
  self.bonus = nil
  self.bonuss = {}
  self.boosts = {}
  
  -- load map
  self.mapFile = require("assets/tiled/map".._num) 

  local tilesetName = self.mapFile.tilesets[1].name
  self.tilesheet = love.graphics.newImage("assets/spritesheets/light2.png")
  self.level = self.mapFile.layers[1].data
  self.flyboost = self.mapFile.layers[2].objects
  self.bonus = self.mapFile.layers[3].objects
  

  local tileNumber = Vector:new(
    self.tilesheet:getWidth() / TW,
    self.tilesheet:getHeight() / TH
  )

  -- parcourt tileset
  for line=0,tileNumber.y - 1,1 do
    for coll=0, tileNumber.x - 1,1  do
      local quad = love.graphics.newQuad(
        coll*TW,line*TH,
        TW,TH,
        self.tilesheet:getDimensions()
      )
      table.insert(self.lstQuad, quad)
    end
  end
  
  -- create tiles
  local id = 1
  for ligne=1, self.mapFile.height do
    for coll=1, self.mapFile.width do
      local numberItem = self.level[((ligne -1) * self.mapFile.width + coll)]
      local myTile = Tile:new(
        (coll-1)*TW, (ligne-1)*TH,
        self.tilesheet, self.lstQuad[numberItem],
        numberItem,
        id,
        tileNumber
      )
      if numberItem ~= 0 and  numberItem ~= 4 and numberItem ~= 5 and numberItem  ~= 9 and numberItem  ~= 10  and numberItem  ~= 14  and numberItem  ~= 15  and numberItem  ~= 16 then myTile.isSolid = true end
      table.insert(self.lstTile, myTile)
      id = id + 1
    end
  end


  -- create bonus
 
  for i,v in ipairs(self.bonus) do
    local bonus = Sprite:new(v.x - TW/2, v.y - TH/2, love.graphics.newImage("assets/spritesheets/life.png"), 3, 4)
    bonus:addAnim("bonus", 7, 9, 0.6)
    bonus:playAnim("bonus")
    table.insert( self.bonuss, bonus )
  end

  -- create boost
  
  for i,v in ipairs(self.flyboost) do
    local boost = Sprite:new(v.x - TW/2, v.y - TH/2, love.graphics.newImage("assets/spritesheets/life.png"), 3, 4)
    boost:addAnim("flyboost", 1, 3, 0.6)
    boost:playAnim("flyboost")
    table.insert( self.boosts, boost )
  end

end

function MapManager:getStartTile(dt)
  for i,v in ipairs(self.lstTile) do
    if v.num == 4 then return v end
  end
end

function MapManager:getDeadTiles(dt)
  local tiles = {}
  for i,v in ipairs(self.lstTile) do
    if v.num == 5 then table.insert(tiles, v) end
  end
  return tiles
end

function MapManager:getEndTile(dt)
  for i,v in ipairs(self.lstTile) do
    if v.num == 10 then return v end
  end
end

function MapManager:update(dt)

  for i,v in ipairs(self.boosts) do
    v:update(dt)
  end

  for i,v in ipairs(self.bonuss) do
    v:update(dt)
  end
end

function MapManager:draw()
  for i,v in ipairs(self.lstTile) do
    v:draw()
  end
end

function MapManager:drawBack()
  for i,v in ipairs(self.boosts) do
    v:draw()
  end
  for i,v in ipairs(self.bonuss) do
    v:draw()
  end
end

function MapManager:getTileAt(position)
  for i,v in ipairs(self.lstTile) do
    if isMouseInBox(
      position.x,
      position.y,
      v.position.x,
      v.position.y,
      v.dimension.x,
      v.dimension.y
    ) then
      return v 
    end
  end
end


return MapManager

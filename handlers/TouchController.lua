local Button = require("objects/Button")
local Vector = require("objects/Vector")

local TouchController = {}
TouchController.__index = TouchController

local bw, bh = 200, 100
local space = 30 

function TouchController:new(pPressedLeft, pReleasedLeft, pPressedRight, pReleasedRight, pPressedUp, pReleasedUp)
  local self = {}
  self.leftButton = Button:new(
    "<",
    Vector:new(space, HEIGHT-bh-space),
    Vector:new(bw, bh),
    pPressedLeft, pReleasedLeft
  )

  self.rightButton = Button:new(
    ">",
    Vector:new(bw+space+space, HEIGHT-bh-space),
    Vector:new(bw, bh),
    pPressedRight, pReleasedRight
  )

  self.jumpButton = Button:new(
    "jump",
    Vector:new(WIDTH - space - bw, HEIGHT-bh-space),
    Vector:new(bw, bh),
    pPressedUp, pReleasedUp
  )

  self.menu = Button:new(
    "menu",
    Vector:new(space, space),
    Vector:new(bw/2, bh/2),
    function()  end, function() sm:setScene("Menu")  end
  )

  setmetatable(self, TouchController)
  return self
end

function TouchController:update(dt)
end

function TouchController:draw()
  love.graphics.setColor(0,0,0, 0.8)
  self.leftButton:draw()
  self.rightButton:draw()
  self.jumpButton:draw()
  self.menu:draw()
  love.graphics.setColor(1,1,1)
end

function TouchController:mousepressed( x, y, isTouch )
  -- self.leftButton:mousepressed(x, y, isTouch)
  -- self.rightButton:mousepressed(x, y, isTouch)
  -- self.jumpButton:mousepressed(x, y, isTouch)
  self.menu:mousepressed(x, y, isTouch)
  return self.leftButton.isPressed, self.rightButton.isPressed, self.jumpButton.isPressed
end

function TouchController:mousereleased( x, y, isTouch )
  -- self.leftButton:mousereleased(x, y, isTouch)
  -- self.rightButton:mousereleased(x, y, isTouch)
  -- self.jumpButton:mousereleased(x, y, isTouch)
  self.menu:mousereleased(x, y, isTouch)
  return self.leftButton.isPressed, self.rightButton.isPressed, self.jumpButton.isPressed
end

function TouchController:touchpressed(id, x, y, dx, dy, pressure )
  self.leftButton:touchpressed(id, x, y, dx, dy, pressure )
  self.rightButton:touchpressed(id, x, y, dx, dy, pressure )
  self.jumpButton:touchpressed(id, x, y, dx, dy, pressure )
  return self.leftButton.isPressed, self.rightButton.isPressed, self.jumpButton.isPressed
end

function TouchController:touchreleased(id, x, y, dx, dy, pressure )
  self.leftButton:touchreleased(id, x, y, dx, dy, pressure )
  self.rightButton:touchreleased(id, x, y, dx, dy, pressure )
  self.jumpButton:touchreleased(id, x, y, dx, dy, pressure )
  return self.leftButton.isPressed, self.rightButton.isPressed, self.jumpButton.isPressed
end

return TouchController
 
local SM = require("handlers/SceneManager")
local Vector = require("objects/Vector")
push = require "push"

local love_admob = require("love_admob")


-- global variables
SLOWMOTION = false
ACTUAL_LEVEL = 1
MAX_LEVEL = 1
WIDTH, HEIGHT = 2000/2, 1080/2
SW, SH = WIDTH/20, HEIGHT/10
TW, TH = 16,16

-- setup push
screenWidth, screenHeight = love.graphics.getDimensions()

push:setupScreen(WIDTH, HEIGHT, screenWidth, screenHeight, { fullscreen = true, resizable = false })

-- global objects
sm = SM:new()
touches = {}

function love.load()
  soundTheme = love.audio.newSource("assets/sounds/theme.wav", "stream", true)
  soundJump = love.audio.newSource("assets/sounds/jump.wav", "static")
  soundRun = love.audio.newSource("assets/sounds/run.wav", "static")
  soundGlisseA = love.audio.newSource("assets/sounds/glisse_a.wav", "static")
  soundGlisseB = love.audio.newSource("assets/sounds/glisse_b.wav", "static")

  soundTheme:play()
  love.graphics.setDefaultFilter("nearest", "nearest")
  love.graphics.setBackgroundColor(1,1,1,1)
  push:setBorderColor(1,1,1,1)
  sm:load()
end

function love.update(dt)
  if not soundTheme:isPlaying( ) then
		soundTheme:play()
	end
  sm:update(dt)
end

function love.draw()
  push:start()
  
  sm:draw()
  push:finish()
end

function love.keypressed(key)
  sm:keypressed(key)
end

function love.keyreleased(key)
  sm:keyreleased(key)
end

function love.mousepressed(x,y)
  sm:mousepressed(push:toGame(x, y))
end

function love.mousereleased(x,y)
  sm:mousereleased(push:toGame(x, y))
end

function love.touchpressed(id, x, y, dx, dy, pressure )
  local x, y = push:toGame(x, y)
  touches[id] = Vector:new(x, y)
  sm:touchpressed(id, x, y, dx, dy, pressure )
end

function love.touchreleased(id, x, y, dx, dy, pressure )
  local x, y = push:toGame(x, y)
  touches[id] = nil
  sm:touchreleased(id, x, y, dx, dy, pressure )
end

function love.resize(w, h)
  return push:resize(w, h)
end

function love.run()
	if love.load then love.load(love.arg.parseGameArguments(arg), arg) end
	if love.timer then love.timer.step() end

	local dt = 0

	return function()
		-- Process events.
		if love.event then
			love.event.pump()
			for name, a,b,c,d,e,f in love.event.poll() do
				if name == "quit" then
					if not love.quit or not love.quit() then
						return a or 0
					end
				end
				love.handlers[name](a,b,c,d,e,f)
			end
		end
		if love.timer then dt = love.timer.step() end
		if love.update then love.update(dt) end -- will pass 0 if love.timer is disabled
		if love.graphics and love.graphics.isActive() then
			love.graphics.origin()
			love.graphics.clear(love.graphics.getBackgroundColor())
			if love.draw then love.draw() end
			love.graphics.present()
		end

		if love_admob then love_admob.update(dt) end
		if love.timer then love.timer.sleep(0.001) end
	end
end
